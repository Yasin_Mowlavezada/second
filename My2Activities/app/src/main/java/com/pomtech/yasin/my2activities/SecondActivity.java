package com.pomtech.yasin.my2activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Intent intent = getIntent();

        TextView nameField2 , ageField2 , descriptionField2;

        nameField2 = (TextView) findViewById(R.id.name_field2);
        ageField2 = (TextView) findViewById(R.id.age_field2);
        descriptionField2 = (TextView) findViewById(R.id.description_field2);

        nameField2.setText(intent.getStringExtra("name"));
        ageField2.setText(intent.getStringExtra("age"));
        descriptionField2.setText(intent.getStringExtra("description"));
    }
}
