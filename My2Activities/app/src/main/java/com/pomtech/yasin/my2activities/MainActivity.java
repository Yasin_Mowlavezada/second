package com.pomtech.yasin.my2activities;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Filling the array in the Resources folder for ages
//        for(int i = 1 ; i < 200 ; i++){
//            getResources().getStringArray(R.array.ages)[i] = String.valueOf(i);
//        }

        final EditText nameField = (EditText) findViewById(R.id.name_field);
        final EditText ageField = (EditText) findViewById(R.id.age_field);
        final EditText descriptionField = (EditText) findViewById(R.id.description_field);
        Button btn = (Button) findViewById(R.id.btn);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this , SecondActivity.class);
                String nameString , ageString , descriptionString;

                nameString = nameField.getText().toString();
                ageString = String.valueOf(ageField.getText().toString());
                descriptionString = descriptionField.getText().toString();
//
//                if (nameString.trim().equalsIgnoreCase("") ){
//                    nameField.setText("Fill this");
//                }else if( ageString.trim().equalsIgnoreCase("")){
//                    ageField.setText("Fill this");
//                }else if( descriptionString.trim().equalsIgnoreCase("")){
//                    descriptionField.setText("Fill this");
//                }else {

                    intent.putExtra("name", nameString);
                    intent.putExtra("age", ageString);
                    intent.putExtra("description", descriptionString);

                    startActivity(intent);
//                }

            }
        });

    }
}
